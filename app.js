const clText = document.getElementById('clText')
const listen = document.getElementById('lnkGtTr1')

document.addEventListener('keydown', (e) => {
  const key = e.code.slice(-1).toLowerCase()

  if (e.altKey && key === 'd') {
    e.preventDefault()
    clText.click()
  } else if (e.altKey && key === 'k') {
    e.preventDefault()
    listen.focus()
  }
})
